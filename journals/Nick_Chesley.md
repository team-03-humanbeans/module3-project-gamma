## 6-26-23

fork/clone project from repo, started group in GIT, and created own working branch

## 6-27-23

finished user stories, began working on backend Authentication

## 6-28-23

finished Auth as a group, more project planning, merging all branches and setting up our own signing keys

## 6-29-23

finished create post/get all posts function as group, moving forward with more functions

## 6-30-23

finished get all posts from specific user as group, prepping a plan for next steps moviong forward post break

## 7-10-23

finished update post, get one post, and delete post functions as a group, planning for user profile backend tomorrow

## 7-11-23

finished all of user backend

## 7-12-23

rewatching videos and going through course material prepping for frontend

## 7-13-23

watching redux videos and going through material

## 7-14-23

continuing videos on redux and frontend auth

## 7-17-23

working on frontend auth as a group, setting up homepage and working on more frontend

## 7-18-23

touching up some things on profile card, post card, other users page

## 7-19-23

working as group on edit post form among other small fixes

## 7-20-23

recreated signup and create profile pages as a group to use redux now and is functioning

## 7-24-23

created edit profile page, got mutiple buttons, links, and navigation elements functioning and visible. error handling on editing a post and a little bit of cleanup.

## 7-25-23

rewatched videos and material on writing unit tests. started and finished as a group writing our unit tests and work on README.md

## 7-26-23

debugging, error handling, redirecting, styling, password confirmation, moved edit post button to postcard, optimization, timing issues and merging everyones work together to main.

## 7-27-23

debugging, error handling, frontend work to prettify our app

## 7-28-23

fixing small breaks in code, removing console logs, print statements, comments, finishing touches on frontend looks, one more unit test added, one api-doc addition, README updated, some CI/CD work to get pipeline running. Done.
