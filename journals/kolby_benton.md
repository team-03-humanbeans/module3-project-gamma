Made dev branch by name

## 6/28

Finally finished Auth and have it fully functional, Auth was a group effort. We also got our database set up, we decided for this project we are going to use PostgreSQL.

## 6/29

Today we succesfully added a create post function in queries and routers. This function is protected from users who are not logged in.

## 6/30

Succesfully created a function that gets all posts from specific users. And returns errors if account does not exist or no posts if account has none.

## 7/10

Today we finished up two endpoints, those being: Update post and Deleting posts. Both protected by needing to be logged in and the logged in account assocciated with the posts.

## 7/11

finished endpoints for create, update, & get user profile

## 7/12

Rewatched redux videos and lectures. As well as brushed up our inner join

### 7/13 - 7/14

watched more rtx videos

## 7/19

git super stuck on multistep signup form and will pick back up tomorrow to get back into fixing it.

## 7/20

redid the signup page and create profile page, did away with multistep form. signup and profile form pages also now use redux.

## 7/24

fixed and implemeneted a few features such as: logout nav, homepage button on nav, edit profile button now works, and some other features.

## 7/25

finished up profile unit tests

## 7/26

added more cancel buttons, and rearranged said buttons. also did some styling.

## 7/27

brushed up our frontend and did a bit more error handling 

## 7/28 
finished up last unit test and last minute error handling