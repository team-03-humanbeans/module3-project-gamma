// import { useEffect, useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import HomePage from "./Homepage.jsx";
import CreatePostForm from "./CreatePostForm.jsx";
import EditPostForm from "./EditPostForm.jsx";
import EditProfileForm from "./EditProfileForm.jsx";
import OtherUsersPage from "./OtherUsersPage.jsx";
import LoginForm from "./LoginForm.jsx";
import MyPage from "./MyPage.jsx";
// import Construct from "./Construct.js";
// import ErrorNotification from "./ErrorNotification";
import "./App.css";
import ProtectedRoutes from "./ProtectedRoutes.js";
import HomePageRedirectRoutes from "./HomePageRedirectRoutes.js";
import SignupPage from "./Signup/SignupPage.jsx";
import ProfileForm from "./Signup/ProfileForm.jsx";
import Nav from "./Nav";
import ProfileEditRedirectRoutes from "./ProfileEditRedirectRoutes.js";
// commented out code will be utilized for deployment
const domain = /https:\/\/[^/]+/;
const basename = process.env.PUBLIC_URL.replace(domain, '');

const App = () => {
  return (
    <BrowserRouter basename={basename}>
      <Nav />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route
          path="/posts/create/"
          element={<ProtectedRoutes child={<CreatePostForm />} />}
        />
        <Route
          path="/posts/:post_id/update/"
          element={<ProtectedRoutes child={<EditPostForm />} />}
        />
        <Route
          path="/profile/edit/"
          element={<ProtectedRoutes child={<EditProfileForm />} />}
        />
        <Route
          path="/mypage/"
          element={<ProtectedRoutes child={<MyPage />} />}
        />
        <Route
          path="/accounts/:account_id/posts/"
          element={<ProtectedRoutes child={<OtherUsersPage />} />}
        />
        <Route
          path="/login/"
          element={<HomePageRedirectRoutes child={<LoginForm />} />}
        />
        <Route
          path="/signup/"
          element={<HomePageRedirectRoutes child={<SignupPage />} />}
        />
        <Route
          path="/profile/"
          element={<ProfileEditRedirectRoutes child={<ProfileForm />} />}
        />
      </Routes>
    </BrowserRouter>
  );
  // const [launchInfo, setLaunchInfo] = useState([]);
  // const [error, setError] = useState(null);

  // useEffect(() => {
  //   async function getData() {
  //     let url = `${process.env.REACT_APP_API_HOST}/api/launch-details`;
  //     console.log("fastapi url: ", url);
  //     let response = await fetch(url);
  //     console.log("------- hello? -------");
  //     let data = await response.json();

  //     if (response.ok) {
  //       console.log("got launch data!");
  //       setLaunchInfo(data.launch_details);
  //     } else {
  //       console.log("drat! something happened");
  //       setError(data.message);
  //     }
  //   }
  //   getData();
  // }, []);

  // return (
  //   <div>
  //     <ErrorNotification error={error} />
  //     <Construct info={launchInfo} />
  //   </div>
  // );
};

export default App;
