import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import {
  useGetUserPostsQuery,
  useGetSpecificUserProfileQuery,
} from "./app/apiSlice";
import PostCard from "./PostCard";
import ProfileCard from "./ProfileCard";

const OtherUsersPage = () => {
  const { account_id } = useParams();
  const { data: postData, isLoading: postLoading } =
    useGetUserPostsQuery(account_id);
  const { data: profileData, isLoading: profileLoading } =
    useGetSpecificUserProfileQuery(account_id);
  const [profileLookup, setProfileLookup] = useState({});

  useEffect(() => {
    if (!profileLoading && profileData) {
      const lookup = {};
      if (typeof profileData === "object") {
        lookup[profileData.account_id] = profileData;
        setProfileLookup(lookup);
      }
    }
  }, [profileLoading, profileData]);

  if (postLoading || profileLoading) return <div>Loading...</div>;

  const hasProfile = !("ERROR!" in profileData);

  return (
    <div className="container-fluid">
      <div className="bg-custom"></div>
      <div className='background-accent shadow-lg'></div>
      <div className='background-accent-2 shadow-lg'></div>
      <div>
        {hasProfile ? (
          <ProfileCard
            profile_picture={profileData.profile_picture}
            pet_name={profileData.pet_name}
            bio={profileData.bio}
            breed={profileData.breed}
            birthday={profileData.birthday}
            gender={profileData.gender}
          />
        ) : (
          <div style={{ fontSize: "1.4rem" }}>
            User hasn't created a profile yet 😢
          </div>
        )}
      </div>
      <div className="my-5 d-flex flex-column align-items-center">
        {postData && Array.isArray(postData) && postData.length > 0 ? (
          postData.map((post) => (
            <div key={post.id} className="mt-4">
              {profileLookup[post.account_id] ? (
                <PostCard
                  profilePicture_url={
                    profileLookup[post.account_id].profile_picture
                  }
                  pet_name={profileLookup[post.account_id].pet_name}
                  picture_url={post.picture_url}
                  description={post.description}
                  post_date={post.post_date}
                  account_id={post.account_id}
                />
              ) : null}
            </div>
          ))
        ) : (
          <div style={{ fontSize: "1.4rem" }}>
            User doesn't have any posts yet 😢
          </div>
        )}
      </div>
    </div>
  );
};

export default OtherUsersPage;
