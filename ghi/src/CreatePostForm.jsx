import { useState } from "react";
import { useCreatePostMutation } from "./app/apiSlice";
import { useNavigate } from "react-router-dom";

const CreatePostForm = () => {
  const [create] = useCreatePostMutation();
  const [picture_url, setPictureUrl] = useState("");
  const [description, setDescription] = useState("");
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    create({ picture_url, description });
    navigate("/mypage");
  };
  const cancelButton = () => {
    navigate(`/mypage`);
  };

  return (
    <div className="container">
      <div className="background-accent shadow-lg"></div>
      <div className="background-accent-2 shadow-lg"></div>
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new post</h1>
            <form onSubmit={handleSubmit} id="create-post-form">
              <div className="form-floating mb-3">
                <input
                  onChange={(e) => setPictureUrl(e.target.value)}
                  value={picture_url}
                  placeholder="Picture URL"
                  required
                  type="text"
                  name="picture_url"
                  id="picture_url"
                  className="form-control"
                  maxLength={500}
                />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="form-floating mb-3">
                <textarea
                  onChange={(e) => setDescription(e.target.value)}
                  value={description}
                  placeholder="Description"
                  required
                  type="text"
                  name="description"
                  id="description"
                  className="form-control"
                  maxLength={300}
                />
                <label htmlFor="description">Description</label>
              </div>
              <button type="submit" className="btn-transition-sm gradient">
                Create
              </button>
              <button
                className="btn-transition-sm gradient"
                onClick={cancelButton}
              >
                Cancel
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreatePostForm;
