import { configureStore } from "@reduxjs/toolkit";
import { fetchrApi } from "./apiSlice";
import { setupListeners } from "@reduxjs/toolkit/query";

export const store = configureStore({
  reducer: {
    [fetchrApi.reducerPath]: fetchrApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(fetchrApi.middleware),
});

setupListeners(store.dispatch);
