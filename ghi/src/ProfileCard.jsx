import { useState } from 'react';

const ProfileCard = ({ profile_picture, pet_name, bio, breed, birthday, gender }) => {
  const [showMore, setShowMore] = useState(false);
  const showMoreButton = bio.length >= 100;
  const currentDate = new Date();
  const birthDate = new Date(birthday);
  const ageInMilliseconds = currentDate - birthDate;
  const ageInYears = Math.floor(ageInMilliseconds / (365.25 * 24 * 60 * 60 * 1000));

  const formatBirthday = (birthday) => {
    const date = new Date(birthday);
    const day = date.getDate();
    const suffix = getDayNumberSuffix(day);

    const options = { month: "long", day: "numeric" };
    return `${date.toLocaleDateString("en-US", options)}${suffix}, ${date.getFullYear()}`;
  };

  const getDayNumberSuffix = (day) => {
    if (day >= 11 && day <= 13) {
      return "th";
    }
    switch (day % 10) {
      case 1:
        return "st";
      case 2:
        return "nd";
      case 3:
        return "rd";
      default:
        return "th";
    }
  };

  return (
    <div className='container'>
    <div className="row justify-content-center">
      <div className='col-5 py-4 d-flex align-items-start border-bottom' style={{width: '40vw', height: '50vh'}}>
        <div style={{ position: "relative", flex: "0 0 20rem", marginRight: "1rem" }}>
          <img alt="" src={profile_picture} className="rounded-circle shadow profile-pic-border" style={{ height: "20vw", width: "20vw", objectFit: 'fill' }} />
        </div>
        <div className="card-body d-flex flex-column justify-content-between align-self-center" style={{ border: "none", flex: "1" }}>
          <div className=''>
            <h5 className="card-title fs-4 mb-4">
              {pet_name} ({ageInYears} years old)
            </h5>
            <p className="card-text">
              <strong>Birthday:</strong> {formatBirthday(birthday)}
            </p>
            <p className="card-text">
              <strong>Breed:</strong> {breed}
            </p>
            <p className="card-text">
              <strong>Gender:</strong> {gender}
            </p>
            <p className="card-text" style={{ wordWrap: "break-word", maxWidth: "300px" }}>
              <strong>Bio: </strong>
              {showMore ? bio : `${bio.substring(0, 100)}`}
              {showMoreButton && (
                <button className='text-muted text-sm-left btn-readmore' onClick={() => setShowMore(!showMore)}>
                  {showMore ? "less" : "more"}
                </button>
              )}
            </p>
          </div>
        </div>
      </div>
    </div>
    </div>
  );
}

export default ProfileCard;
