steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE likes (
            id SERIAL PRIMARY KEY NOT NULL,
            likes SERIAL NOT NULL,
            post_id SERIAL NOT NULL,
            account_id SERIAL NOT NULL,
            user_who_liked_id SERIAL NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE likes;
        """,
    ],
]
