from fastapi.testclient import TestClient
from main import app
from queries.posts import PostQueries
from authenticator import authenticator
from models.posts import PostIn, PostInUpdate

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "71", "username": "fake-user"}


class FakePostQueries:
    def get_all(self):
        return [
            {
                "description": "test 100011",
                "picture_url": "string",
                "id": 1,
                "account_id": 71,
                "account_username": "test_1",
                "post_date": "2023-06-29",
            },
        ]

    def get_user_posts(self, account_id: int):
        return [
            {
                "description": "test 100011",
                "picture_url": "string",
                "id": 1,
                "account_id": account_id,
                "account_username": "test_1",
                "post_date": "2023-06-29",
            }
        ]

    def get_one(self, post_id: int):
        return {
                "description": "test 100011",
                "picture_url": "string",
                "id": post_id,
                "account_id": 71,
                "account_username": "test_1",
                "post_date": "2023-06-29",
            }

    def create(self, post: PostIn, account_data: dict):
        post = post.dict()
        post["id"] = "23"
        post["account_id"] = account_data["id"]
        post["post_date"] = "2023-06-29"
        post["account_username"] = account_data["username"]
        return post

    def update(self, post_id: int, post: PostInUpdate, account_data: dict):
        post = post.dict()
        post["id"] = post_id
        post["picture_url"] = "unit tests"
        post["account_id"] = account_data["id"]
        post["post_date"] = "2023-06-29"
        post["account_username"] = account_data["username"]
        return post

    def delete(self, post_id: int, account_data: dict,):
        return True


def test_get_posts_all():
    app.dependency_overrides[PostQueries] = FakePostQueries

    res = client.get("/posts")
    data = res.json()

    assert res.status_code == 200
    assert data == [
        {
            "description": "test 100011",
            "picture_url": "string",
            "id": 1,
            "account_id": 71,
            "account_username": "test_1",
            "post_date": "2023-06-29",
        }
    ]


def test_get_user_posts():
    app.dependency_overrides[PostQueries] = FakePostQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/accounts/71/posts")
    data = res.json()

    assert res.status_code == 200
    assert data == [
        {
            "description": "test 100011",
            "picture_url": "string",
            "id": 1,
            "account_id": 71,
            "account_username": "test_1",
            "post_date": "2023-06-29",
        }
    ]


def test_get_my_posts():
    app.dependency_overrides[PostQueries] = FakePostQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.get("/myposts")
    data = res.json()

    assert res.status_code == 200
    assert data == [
        {
            "description": "test 100011",
            "picture_url": "string",
            "id": 1,
            "account_id": "71",
            "account_username": "test_1",
            "post_date": "2023-06-29",
        }
    ]


def test_get_one_post():
    app.dependency_overrides[PostQueries] = FakePostQueries
    res = client.get("/posts/1")
    data = res.json()

    assert res.status_code == 200
    assert data == {
            "description": "test 100011",
            "picture_url": "string",
            "id": 1,
            "account_id": 71,
            "account_username": "test_1",
            "post_date": "2023-06-29",
        }


def test_create_post():
    app.dependency_overrides[PostQueries] = FakePostQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    post = {
            "description": "testing version",
            "picture_url": "unit tests",
    }
    res = client.post("/posts", json=post)
    data = res.json()
    assert res.status_code == 200
    assert data == {
             "description": "testing version",
             "picture_url": "unit tests",
             "id": "23",
             "account_id": "71",
             "account_username": "fake-user",
             "post_date": "2023-06-29",
    }


def test_update_post():
    app.dependency_overrides[PostQueries] = FakePostQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    post = {
        "description": "testing version"
    }
    res = client.put('/posts/23', json=post)
    data = res.json()
    assert res.status_code == 200
    assert data == {
             "description": "testing version",
             "picture_url": "unit tests",
             "id": 23,
             "account_id": "71",
             "account_username": "fake-user",
             "post_date": "2023-06-29",
    }


def test_delete_post():
    app.dependency_overrides[PostQueries] = FakePostQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data

    res = client.delete("/posts/45")
    data = res.json()

    assert res.status_code == 200
    assert data is True
