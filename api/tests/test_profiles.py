from fastapi.testclient import TestClient
from main import app
from queries.profiles import ProfileQueries
from authenticator import authenticator
from models.profiles import ProfileIn

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "71", "username": "fake-user"}


class FakeProfileQueries:
    def create(
        self,
        profile: ProfileIn,
        account_data: dict,
    ):
        profile = profile.dict()
        profile["id"] = "1"
        profile["account_id"] = account_data["id"]

        return profile

    def get_self(self, account_data: dict):
        return {
            "profile_picture": "test.jpg",
            "pet_name": "test",
            "birthday": "1988-06-06",
            "gender": "test",
            "breed": "Neapolitan Mastiff",
            "bio": "test",
            "id": 1,
            "account_id": account_data["id"],
        }

    def get_one(self, account_id: int):
        return {
            "profile_picture": "test.jpg",
            "pet_name": "test",
            "birthday": "1988-06-06",
            "gender": "test",
            "breed": "Neapolitan Mastiff",
            "bio": "test",
            "id": 1,
            "account_id": account_id,
        }

    def update(
        self,
        profile: ProfileIn,
        account_data: dict,
    ):
        profile = profile.dict()
        profile["id"] = 1
        profile["account_id"] = account_data["id"]
        return profile

    def get_all(self):
        return [{
            "profile_picture": "test.jpg",
            "pet_name": "test",
            "birthday": "1988-06-06",
            "gender": "test",
            "breed": "Neapolitan Mastiff",
            "bio": "test",
            "id": 1,
            "account_id": 71,
        }]


def test_create_profile():
    app.dependency_overrides[ProfileQueries] = FakeProfileQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    profile = {
        "profile_picture": "test",
        "pet_name": "testname",
        "birthday": "2023-07-25",
        "gender": "test",
        "breed": "test",
        "bio": "test",
    }
    res = client.post("/profile", json=profile)
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "profile_picture": "test",
        "pet_name": "testname",
        "birthday": "2023-07-25",
        "gender": "test",
        "breed": "test",
        "bio": "test",
        "id": "1",
        "account_id": "71",
    }


def test_update_profile():
    app.dependency_overrides[ProfileQueries] = FakeProfileQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    profile = {
        "profile_picture": "test",
        "pet_name": "testname",
        "birthday": "2023-07-25",
        "gender": "test",
        "breed": "test",
        "bio": "test",
    }
    res = client.put("/profile", json=profile)
    data = res.json()
    assert res.status_code == 200
    assert data == {
        "profile_picture": "test",
        "pet_name": "testname",
        "birthday": "2023-07-25",
        "gender": "test",
        "breed": "test",
        "bio": "test",
        "id": 1,
        "account_id": "71",
    }


def test_get_current_logged_in_profile():
    app.dependency_overrides[ProfileQueries] = FakeProfileQueries
    res = client.get("/profile")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "profile_picture": "test.jpg",
        "pet_name": "test",
        "birthday": "1988-06-06",
        "gender": "test",
        "breed": "Neapolitan Mastiff",
        "bio": "test",
        "id": 1,
        "account_id": "71",
    }


def test_get_specific_user_profile():
    app.dependency_overrides[ProfileQueries] = FakeProfileQueries
    res = client.get("/profiles/71")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "profile_picture": "test.jpg",
        "pet_name": "test",
        "birthday": "1988-06-06",
        "gender": "test",
        "breed": "Neapolitan Mastiff",
        "bio": "test",
        "id": 1,
        "account_id": 71,
    }


def test_get_all_profiles():
    app.dependency_overrides[ProfileQueries] = FakeProfileQueries
    res = client.get("/profiles")
    data = res.json()

    assert res.status_code == 200
    assert data == [{
        "profile_picture": "test.jpg",
        "pet_name": "test",
        "birthday": "1988-06-06",
        "gender": "test",
        "breed": "Neapolitan Mastiff",
        "bio": "test",
        "id": 1,
        "account_id": 71,
    }]
