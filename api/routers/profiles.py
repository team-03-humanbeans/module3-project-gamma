from fastapi import (
    Depends,
    APIRouter
)
from authenticator import authenticator
from queries.profiles import ProfileQueries
from models.profiles import ProfileIn, ProfileOut
from typing import List, Union
from models.error_handler import Error

router = APIRouter()


@router.post("/profile")
def create_profile(
    profile: ProfileIn,
    queries: ProfileQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.create(profile, account_data)


@router.put("/profile")
def update_profile(
    profile: ProfileIn,
    queries: ProfileQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    return queries.update(profile, account_data)


@router.get("/profile")
def get_current_logged_in_profile(
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ProfileQueries = Depends(),
):
    return queries.get_self(account_data)


@router.get("/profiles/{account_id}")
def get_specific_user_profile(
    account_id: int,
    account_data: dict = Depends(authenticator.get_current_account_data),
    queries: ProfileQueries = Depends(),
):
    return queries.get_one(account_id)


@router.get("/profiles", response_model=Union[List[ProfileOut], Error])
def get_all_profiles(
    queries: ProfileQueries = Depends(),
):
    return queries.get_all()
