from models.posts import PostIn, PostOut, PostInUpdate
from queries.pool import pool
from datetime import datetime
from typing import List, Union
from models.error_handler import Error


class PostQueries:
    def create(
        self,
        post: PostIn,
        account_data: dict,
    ) -> PostOut:
        post_date = datetime.now()
        account_id = account_data["id"]
        account_username = account_data["username"]
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO posts
                    (post_date, picture_url, description, account_id)
                    VALUES (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        post_date,
                        post.picture_url,
                        post.description,
                        account_id,
                    ],
                )

                id = result.fetchone()[0]
                props = post.dict()
                props["account_id"] = account_id
                props["post_date"] = post_date
                props["account_username"] = account_username
                return PostOut(id=id, **props)

    def get_all(self) -> Union[Error, List[PostOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT p.id, p.post_date, p.picture_url, p.description,
                        p.account_id, a.username
                        FROM posts AS p
                        INNER JOIN accounts AS a
                        ON (a.id = p.account_id)
                        ORDER BY p.post_date DESC;
                        """
                    )

                    result = []
                    for post in db:
                        posts = PostOut(
                            id=post[0],
                            post_date=post[1],
                            picture_url=post[2],
                            description=post[3],
                            account_id=post[4],
                            account_username=post[5],
                        )

                        result.append(posts)
                    return result
        except Exception as e:
            print(e)
            return {"ERROR!": "COULD NOT GET POSTS"}

    def get_user_posts(self, account_id: int) -> List[PostOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                all_accounts = db.execute(
                    """
                    SELECT id
                    FROM accounts
                    """,
                )

                data = all_accounts.fetchall()
                for tuple in data:
                    if account_id not in tuple:
                        continue
                    else:
                        db.execute(
                            """
                            SELECT p.id, p.post_date, p.picture_url,
                            p.description, p.account_id, a.username
                            FROM posts AS p
                            INNER JOIN accounts AS a
                            ON (a.id = p.account_id)
                            WHERE account_id = %s
                            ORDER BY post_date DESC;
                            """,
                            [account_id],
                        )

                        result = []
                        for post in db:
                            posts = PostOut(
                                id=post[0],
                                post_date=post[1],
                                picture_url=post[2],
                                description=post[3],
                                account_id=post[4],
                                account_username=post[5],
                            )

                            result.append(posts)
                        if len(result) == 0:
                            return {"MESSAGE": "USER HAS NO POSTS"}
                        return result
                return {"MESSAGE": "ACCOUNT ID DOES NOT EXIST"}

    def get_one(self, post_id: int) -> PostOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT p.id, p.post_date, p.picture_url, p.description,
                        p.account_id, a.username
                        FROM posts AS p
                        INNER JOIN accounts AS a
                        ON (a.id = p.account_id)
                        WHERE p.id = %s
                        """,
                        [post_id],
                    )

                    data = result.fetchone()
                    return PostOut(
                        id=data[0],
                        post_date=data[1],
                        picture_url=data[2],
                        description=data[3],
                        account_id=data[4],
                        account_username=data[5],
                    )
        except Exception as e:
            print(e)
            return {"ERROR!": "POST DOES NOT EXIST"}

    def update(
        self,
        post_id: int,
        post: PostInUpdate,
        account_data: dict,
    ) -> PostOut:
        try:
            logged_in_account_id = account_data["id"]
            account_username = account_data["username"]
            post_account_id = self.get_one(post_id).account_id
            if post_account_id != logged_in_account_id:
                return {"message": "user is not allowed to update this post"}
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE posts
                        SET description = %s
                        WHERE id = %s
                        """,
                        [post.description, post_id],
                    )

                    props = post.dict()
                    props["account_id"] = logged_in_account_id
                    props["post_date"] = self.get_one(post_id).post_date
                    props["picture_url"] = self.get_one(post_id).picture_url
                    props["account_username"] = account_username
                    return PostOut(id=post_id, **props)
        except Exception as e:
            print(e)
            return {"ERROR!": "POST DOES NOT EXIST"}

    def delete(
        self,
        post_id: int,
        account_data: dict,
    ) -> bool:
        try:
            logged_in_account_id = account_data["id"]
            post_account_id = self.get_one(post_id).account_id
            if post_account_id != logged_in_account_id:
                return {"message": "user is not allowed to delete this post"}
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM posts
                        WHERE id = %s
                        """,
                        [post_id],
                    )

            return True
        except Exception as e:
            print(e)
            return {"ERROR!": "POST DOES NOT EXIST"}
