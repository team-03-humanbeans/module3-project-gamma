from models.profiles import ProfileIn, ProfileOut, DuplicateProfileError
from models.error_handler import Error
from typing import List, Union
from queries.pool import pool


class ProfileQueries:
    def create(
        self,
        profile: ProfileIn,
        account_data: dict,
    ) -> ProfileOut:
        account_id = account_data["id"]
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO profiles
                    (profile_picture,
                    pet_name,
                    birthday,
                    gender,
                    breed,
                    bio,
                    account_id)
                    VALUES (%s, %s, %s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        profile.profile_picture,
                        profile.pet_name,
                        profile.birthday,
                        profile.gender,
                        profile.breed,
                        profile.bio,
                        account_id,
                    ],
                )
                id = result.fetchone()[0]
                props = profile.dict()
                if self.get_one(account_id) != {
                    "ERROR!": "PROFILE DOES NOT EXIST"
                }:
                    raise DuplicateProfileError
                props["account_id"] = account_id
                return ProfileOut(id=id, **props)

    def get_self(self, account_data: dict) -> ProfileOut:
        try:
            account_id = account_data["id"]
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, profile_picture, pet_name, birthday, gender,
                        breed, bio, account_id
                        FROM profiles
                        WHERE account_id = %s
                        """,
                        [account_id],
                    )
                    data = result.fetchone()
                    return ProfileOut(
                        id=data[0],
                        profile_picture=data[1],
                        pet_name=data[2],
                        birthday=data[3],
                        gender=data[4],
                        breed=data[5],
                        bio=data[6],
                        account_id=data[7],
                    )
        except Exception as e:
            print(e)
            return {"ERROR!": "PROFILE DOES NOT EXIST"}

    def get_one(self, account_id: int) -> ProfileOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id, profile_picture, pet_name, birthday, gender,
                        breed, bio, account_id
                        FROM profiles
                        WHERE account_id = %s
                        """,
                        [account_id],
                    )
                    data = result.fetchone()
                    return ProfileOut(
                        id=data[0],
                        profile_picture=data[1],
                        pet_name=data[2],
                        birthday=data[3],
                        gender=data[4],
                        breed=data[5],
                        bio=data[6],
                        account_id=data[7],
                    )
        except Exception as e:
            print(e)
            return {"ERROR!": "PROFILE DOES NOT EXIST"}

    def update(
        self,
        profile: ProfileIn,
        account_data: dict,
    ) -> ProfileOut:
        try:
            logged_in_account_id = account_data["id"]
            profile_account_id = self.get_self(account_data).account_id
            if profile_account_id != logged_in_account_id:
                return {
                    "message": "user is not allowed to update this profile"
                }
            profile_id = self.get_self(account_data).id
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                            UPDATE profiles
                            SET profile_picture = %s
                                , pet_name = %s
                                , birthday = %s
                                , gender = %s
                                , breed = %s
                                , bio = %s
                                WHERE id = %s
                            """,
                        [
                            profile.profile_picture,
                            profile.pet_name,
                            profile.birthday,
                            profile.gender,
                            profile.breed,
                            profile.bio,
                            profile_id,
                        ],
                    )
                    props = profile.dict()
                    props["account_id"] = profile_account_id
                    return ProfileOut(id=profile_id, **props)
        except Exception as e:
            print(e)
            return {"ERROR!": "ACCESS DENIED"}

    def get_all(self) -> Union[Error, List[ProfileOut]]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT id, profile_picture, pet_name, birthday, gender,
                        breed, bio, account_id
                        FROM profiles
                        """
                    )

                    result = []
                    for profile in db:
                        profiles = ProfileOut(
                            id=profile[0],
                            profile_picture=profile[1],
                            pet_name=profile[2],
                            birthday=profile[3],
                            gender=profile[4],
                            breed=profile[5],
                            bio=profile[6],
                            account_id=profile[7],
                        )

                        result.append(profiles)
                    return result
        except Exception as e:
            print(e)
            return {"ERROR!": "COULD NOT GET PROFILES"}
