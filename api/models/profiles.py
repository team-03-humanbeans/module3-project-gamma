from pydantic import BaseModel
from datetime import date


class DuplicateProfileError(ValueError):
    pass


class ProfileIn(BaseModel):
    profile_picture: str
    pet_name: str
    birthday: date
    gender: str
    breed: str
    bio: str


class ProfileOut(ProfileIn):
    id: int
    account_id: int
